import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Producto } from '../Model/Producto';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  producto: Producto[];

  constructor(private http: HttpClient) { }

  endpoint= "http://localhost:8280/Java/Producto";

  getProducto(){
    return this.http.get<Producto[]>(this.endpoint);
  }

}
