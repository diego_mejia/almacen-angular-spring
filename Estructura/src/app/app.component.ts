import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'Estructura';

  constructor(private router: Router){}

  listar(){
    this.router.navigate(['listar']);
  }
}
