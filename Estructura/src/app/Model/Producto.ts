export class Producto{
    id: number;
    nombre: String;
    codigo: String;
    stock: number;
    precioCompra: number;
    precioVenta: number;
}