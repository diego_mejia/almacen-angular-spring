package com.pe.edu.unmsm.ProyectoED.interfaces;

import com.pe.edu.unmsm.ProyectoED.model.Producto;
import com.pe.edu.unmsm.ProyectoED.structure.ListaDoble;
import org.springframework.data.repository.Repository;

public interface ProductoRepositorio extends Repository<Producto, String>{
    ListaDoble<Producto> findAll();
    Producto findOne(String codigo);
    Producto save(Producto p);
    void delete(Producto p);
}
