package com.pe.edu.unmsm.ProyectoED.service;

import com.pe.edu.unmsm.ProyectoED.interfaces.ProductoRepositorio;
import com.pe.edu.unmsm.ProyectoED.interfaces.ProductoService;
import com.pe.edu.unmsm.ProyectoED.model.Producto;
import com.pe.edu.unmsm.ProyectoED.structure.ListaDoble;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductoServiceImpl implements ProductoService {

    @Autowired
    private ProductoRepositorio repositorio;

    @Override
    public ListaDoble<Producto> listar() {
        return repositorio.findAll();
    }

    @Override
    public Producto listarCodigo(String codigo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Producto add(Producto p) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Producto edit(Producto p) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Producto delete(String codigo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
