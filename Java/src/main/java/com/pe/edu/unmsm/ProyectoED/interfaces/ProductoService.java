package com.pe.edu.unmsm.ProyectoED.interfaces;

import com.pe.edu.unmsm.ProyectoED.model.Producto;
import com.pe.edu.unmsm.ProyectoED.structure.ListaDoble;

public interface ProductoService {
    ListaDoble<Producto> listar();
    Producto listarCodigo(String codigo);
    Producto add(Producto p);
    Producto edit(Producto p);
    Producto delete(String codigo);
}
